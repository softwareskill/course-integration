## basket-service-cloud

Serwis prezentujący klienta seriwsu Order service

Main class: `BasketServiceCloudApplication`

### Monitoring

1. Uruchom stos centralnego monitoringu.

Postępuj wg instrukcji: [/docker/monitoring/README.md](/docker/monitoring/README.md)

Dashboard: https://grafana.com/grafana/dashboards/12464

2. Uruchom aplikację z profilem

```
-Dspring.profiles.active=monitoring
```

## orders-service-cloud

Serwis prezentujący backend działający w środowisku cloudowym.

Zakłada zamówienia w systemie i odpowiada ze szczegółami.

Main class: `OrdersServiceCloudApplication`

### Service Discovery

Aby zarejestrować serwis w Consul, uruchom aplikację z profilem `consul`:

```
-Dspring.profiles.active=consul
```

### Agregacja logów

Uruchom stos centralnego logowania.

Postępuj wg instrukcji: [/docker/central-logging/README.md](/docker/central-logging/README.md)

#### Agregacja logów bezpośrednio z aplikacji

1. Dodaj parametr VM:

```
-Dlogging.config=classpath:logback-central-logging-gelf.xml
```

#### Agregacja logów przez plik oraz sidecar

1. Dodaj parametr VM:
```
-Dlogging.config=classpath:logback-central-logging-file.xml
```
2. Uruchom obraz dockerowy
```
cd ./central-logging-sidecar
docker-compose up -d
```

### Tracing

1. Uruchom stos tracingu.

Postępuj wg instrukcji: [/docker/zipkin/README.md](/docker/zipkin/README.md)

2. Uruchom aplikację z profilem:

```
-Dspring.profiles.active=tracing
```
