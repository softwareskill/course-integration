package pl.softwareskill.course.microservices.orders.domain;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.locks.LockSupport;

@RequiredArgsConstructor
@Slf4j
public class CreateOrderService {

    private final OrderRepository orderRepository;
    private final Duration delay;

    public Order createOrder(String description) {
        UUID orderId = UUID.randomUUID();
        Order createdOrder = Order.create(orderId, description);

        simulateDelay();

        orderRepository.save(createdOrder);

        return createdOrder;
    }

    private void simulateDelay() {
        if (!delay.isZero()) {
            log.info("Simulating delay by {}", delay);
            LockSupport.parkNanos(delay.toNanos());
        }
    }
}
