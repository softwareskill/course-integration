package pl.softwareskill.course.microservices.orders.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.microservices.orders.domain.CreateOrderService;
import pl.softwareskill.course.microservices.orders.domain.OrderRepository;

import java.time.Duration;

@Configuration
public class OrderConfig {

    @Bean
    public CreateOrderService createOrderService(OrderRepository orderRepository,
        @Value("${order.service.simulated.delay:PT0s}") Duration delay) {
        return new CreateOrderService(orderRepository, delay);
    }
}
