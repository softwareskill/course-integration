package pl.softwareskill.course.microservices.basket.service;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.decorators.Decorators;
import io.github.resilience4j.retry.Retry;
import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
public class ResilientOrderService {

    private final OrderService orderService;
    private final Retry retry;
    private final CircuitBreaker circuitBreaker;

    public Optional<UUID> createOrder(String description) {
        return Decorators.ofSupplier(() -> Optional.of(orderService.createOrder(description)))
                .withCircuitBreaker(circuitBreaker)
                .withRetry(retry)
                .withFallback(e -> Optional.empty())
                .get();
    }
}
