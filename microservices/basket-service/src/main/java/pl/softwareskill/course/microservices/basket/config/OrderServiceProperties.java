package pl.softwareskill.course.microservices.basket.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@Data
@ConfigurationProperties("order.service")
public class OrderServiceProperties {
    String url;
    Duration timeout;
}
