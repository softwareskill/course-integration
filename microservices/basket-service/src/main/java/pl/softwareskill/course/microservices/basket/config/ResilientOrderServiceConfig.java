package pl.softwareskill.course.microservices.basket.config;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.microservices.basket.service.OrderService;
import pl.softwareskill.course.microservices.basket.service.ResilientOrderService;

import java.time.Duration;

@Configuration
@EnableConfigurationProperties(OrderServiceProperties.class)
@Slf4j
public class ResilientOrderServiceConfig {

    @Bean
    public ResilientOrderService resilientOrderService(OrderService orderService, Retry orderServiceRetry) {
        return new ResilientOrderService(orderService, orderServiceRetry, circuitBreaker());
    }

    private CircuitBreaker circuitBreaker() {
        CircuitBreaker circuitBreaker = CircuitBreaker.of("create order", CircuitBreakerConfig.custom()
                .failureRateThreshold(50)
                .minimumNumberOfCalls(3)
                .slidingWindowSize(3)
                .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
                .waitDurationInOpenState(Duration.ofSeconds(5))
                .permittedNumberOfCallsInHalfOpenState(1)
                .build());

        circuitBreaker.getEventPublisher()
                .onEvent(event -> log.info("Circuit Breaker event: {}", event));

        return circuitBreaker;
    }

    @Configuration
    class OrderServiceRetryConfig {

        @Bean(name = "orderServiceRetry")
        @ConditionalOnProperty(value = "orderservice.retry.enabled", havingValue = "true")
        Retry retry() {
            Retry retry = Retry.of("create order", RetryConfig.custom()
                    .maxAttempts(3)
                    .waitDuration(Duration.ofMillis(200))
                    .build());

            retry.getEventPublisher()
                    .onEvent(event -> log.info("Retry event: {}", event));

            return retry;
        }

        @Bean(name = "orderServiceRetry")
        @ConditionalOnProperty(value = "orderservice.retry.enabled", havingValue = "false", matchIfMissing = true)
        Retry noRetry() {
            return Retry.of("create order", RetryConfig.custom()
                    .maxAttempts(1)
                    .build());
        }
    }

}
