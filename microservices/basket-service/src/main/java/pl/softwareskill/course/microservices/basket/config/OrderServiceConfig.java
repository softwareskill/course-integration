package pl.softwareskill.course.microservices.basket.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import pl.softwareskill.course.microservices.basket.service.OrderService;

@Configuration
@EnableConfigurationProperties(OrderServiceProperties.class)
public class OrderServiceConfig {

    @Bean
    public OrderService orderService(OrderServiceProperties config) {
        return new OrderService(restTemplate(config), config.getUrl());
    }

    private RestTemplate restTemplate(OrderServiceProperties config) {
        var factory = new SimpleClientHttpRequestFactory();

        factory.setConnectTimeout((int) config.getTimeout().toMillis());
        factory.setReadTimeout((int) config.getTimeout().toMillis());

        return new RestTemplate(factory);
    }
}
