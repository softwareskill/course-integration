package pl.softwareskill.course.microservices.basket.config;

import io.github.resilience4j.core.IntervalFunction;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class ResilientOrderServiceConfigTest {

    @Test
    void exponentialBackoff() {
        var fn = IntervalFunction.ofExponentialBackoff(Duration.ofMillis(200), 2);

        System.out.println(fn.apply(1));
        System.out.println(fn.apply(2));
        System.out.println(fn.apply(3));
        System.out.println(fn.apply(4));
    }
}