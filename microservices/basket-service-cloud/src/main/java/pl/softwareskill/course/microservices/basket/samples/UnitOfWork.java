package pl.softwareskill.course.microservices.basket.samples;

import io.opentracing.Scope;
import io.opentracing.Span;
import io.opentracing.Tracer;
import io.opentracing.log.Fields;
import io.opentracing.tag.Tags;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.scheduling.annotation.Async;

import java.util.Map;

@Slf4j
public class UnitOfWork {

    @Autowired
    Tracer tracer;

    @SneakyThrows
    public void doSomeWork(String argument) {
        log.info("Doing work {}", argument);

        Span span = tracer.buildSpan("someWork2").start();
        span.setTag("argument", argument);

        try (Scope scope = tracer.scopeManager().activate(span)) {
            Thread.sleep(1000);
        } catch(Exception ex) {
            Tags.ERROR.set(span, true);
            span.log(Map.of(Fields.EVENT, "error", Fields.ERROR_OBJECT, ex, Fields.MESSAGE, ex.getMessage()));
        } finally {
            span.finish();
        }
    }

    @NewSpan
    @SneakyThrows
    public void doSomeWorkMonitored(@SpanTag("argument") String argument) {
        log.info("Doing work {}", argument);
        Thread.sleep(1000);
    }

    @Async
    @SneakyThrows
    public void doSomeAsyncWork(@SpanTag("argument") String argument) {
        log.info("Doing work {}", argument);
        Thread.sleep(1000);
    }
}
