package pl.softwareskill.course.microservices.basket.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.cloud.loadbalancer.support.ServiceInstanceListSuppliers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;

@Configuration
@Profile("!consul")
@LoadBalancerClient(name = "orders-service", configuration = OrderServiceFixedInstancesConfig.class)
public class OrderServiceFixedInstancesConfig {

    public static final String SERVICE_ID = "order-service";

    @Bean
    public ServiceInstanceListSupplier fixedListSupplier(@Value("${order.service.urls}") String[] urls) {
        var instances = new ArrayList<ServiceInstance>(urls.length);
        int i = 1;
        for (String url : urls) {
            instances.add(toInstance(i, url));
            ++i;
        }

        return ServiceInstanceListSuppliers.from(SERVICE_ID, instances.toArray(new ServiceInstance[]{}));
    }

    private DefaultServiceInstance toInstance(int instanceNo, String url) {
        String[] part = url.split(":");
        boolean secure = url.startsWith("https");
        return new DefaultServiceInstance(SERVICE_ID + "-" + instanceNo, SERVICE_ID, part[0], Integer.valueOf(part[1]), secure);
    }

    @Bean
    @LoadBalanced
    public WebClient.Builder orderServiceWebClient() {
        return WebClient.builder();
    }
}
