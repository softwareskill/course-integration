package pl.softwareskill.course.microservices.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersServiceCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrdersServiceCloudApplication.class, args);
    }
}
