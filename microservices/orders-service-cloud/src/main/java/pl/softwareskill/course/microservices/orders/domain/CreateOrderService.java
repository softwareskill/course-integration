package pl.softwareskill.course.microservices.orders.domain;

import lombok.RequiredArgsConstructor;

import java.util.UUID;

@RequiredArgsConstructor
public class CreateOrderService {

    private final OrderRepository orderRepository;

    public Order createOrder(String description) {
        UUID orderId = UUID.randomUUID();
        Order createdOrder = Order.create(orderId, description);

        orderRepository.save(createdOrder);

        return createdOrder;
    }
}
