package pl.softwareskill.course.integration.messaging.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MessageListener {

    @JmsListener(destination = "orders")
    public void onMessage(Message<Object> message) {
        log.info("Received message: {}", message.getPayload());
    }
}
