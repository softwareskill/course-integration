package pl.softwareskill.course.integration.messaging.spring.dto;

import lombok.Data;

@Data
public class Order {
    String id;
    String description;
}
