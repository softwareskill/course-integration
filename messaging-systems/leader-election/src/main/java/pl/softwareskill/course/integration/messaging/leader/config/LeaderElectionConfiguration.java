package pl.softwareskill.course.integration.messaging.leader.config;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.leader.LeaderLatch;
import org.apache.curator.framework.recipes.leader.LeaderLatchListener;
import org.apache.curator.retry.RetryForever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.jms.config.JmsListenerEndpointRegistry;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@Configuration
public class LeaderElectionConfiguration {

    @Autowired
    private GenericApplicationContext applicationContext;

    private CountDownLatch applicationStarted = new CountDownLatch(1);
    private AtomicBoolean isLeader = new AtomicBoolean(false);

    @Bean
    CuratorFramework curatorFramework(@Value("${zookeeper.url}") String url,
                                      @Value("${zookeeper.retry.interval}") Duration retryInterval) throws InterruptedException {
        RetryPolicy retryPolicy = new RetryForever((int) retryInterval.toMillis());
        CuratorFramework curator = CuratorFrameworkFactory.newClient(url, retryPolicy);
        curator.start();
        curator.blockUntilConnected();
        return curator;
    }

    @Bean(initMethod = "start", destroyMethod = "close")
    LeaderLatch leaderLatch(CuratorFramework client,
                            @Value("${zookeeper.leader-election.path}") String path,
                            @Value("${application.uuid}") String applicationId) {
        LeaderLatch leaderLatch = new LeaderLatch(client, path, applicationId, LeaderLatch.CloseMode.NOTIFY_LEADER);
        leaderLatch.addListener(new LeaderLatchListener() {
            @Override
            public void isLeader() {
                onLeaderElected();
            }

            @Override
            public void notLeader() {
                onLeaderStepDown();
            }
        });
        return leaderLatch;
    }

    @SneakyThrows
    void onLeaderElected() {
        log.info("Leader elected");
        applicationStarted.await();

        if (isLeader.compareAndSet(false, true)) {
            JmsListenerEndpointRegistry registry = applicationContext.getBean(JmsListenerEndpointRegistry.class);
            registry.start();
            log.info("Jms started");
        }
    }

    @SneakyThrows
    void onLeaderStepDown() {
        log.info("Leader stepped down");

        if (isLeader.compareAndSet(true, false)) {
            if (applicationContext.isRunning()) {
                JmsListenerEndpointRegistry registry = applicationContext.getBean(JmsListenerEndpointRegistry.class);
                registry.stop();
            }
        }
    }

    @EventListener
    void applicationStarted(ApplicationStartedEvent event) {
        applicationStarted.countDown();
    }

    @EventListener
    void keepSpringAlive(ApplicationStartedEvent event) {
        applicationStarted.countDown();

        // Prevent spring boot application immediately close.
        // Normally once some listener/web interface is exposed it wont be done.
        Executors.newScheduledThreadPool(1)
                .scheduleAtFixedRate(() -> {}, 0, 1, TimeUnit.SECONDS);
    }
}
