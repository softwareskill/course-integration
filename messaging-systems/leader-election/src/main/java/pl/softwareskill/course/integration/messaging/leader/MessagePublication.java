package pl.softwareskill.course.integration.messaging.leader;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStreamReader;

@Slf4j
@SpringBootConfiguration
@EnableAutoConfiguration
public class MessagePublication {

    private final ApplicationContext applicationContext;

    public MessagePublication(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(MessagePublication.class, args);

        new MessagePublication(context).publishMessages();
    }

    @SneakyThrows
    private void publishMessages() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while(true) {
            System.out.println("Enter message:");
            String line = reader.readLine();
            sendMessage(line);
        }
    }

    private void sendMessage(String line) {
        var message = "Order " + line;

        log.info("Sending order {}", message);

        JmsTemplate jmsTemplate = applicationContext.getBean(JmsTemplate.class);
        jmsTemplate.convertAndSend("orders", message);
    }
}
