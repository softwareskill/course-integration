package pl.softwareskill.course.integration.messaging.jms;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.support.JmsUtils;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import java.util.UUID;

@Slf4j
public class JmsPublisher {

    public static void main(String[] args) {
        new JmsPublisher().run();
    }

    public void run() {
        JmsConfig config = new JmsConfig();
        MessageProducer producer = null;

        try {
            var clientId = "JMS Publisher " + UUID.randomUUID();
            config.init(clientId);

            Session session = config.getSession();
            log.info("Creating publisher {}", clientId);

            Destination destination = session.createQueue("orders");
            producer = session.createProducer(destination);

            Message message = session.createTextMessage("message");
            producer.send(message);

            log.info("Message has been sent");
        } catch (JMSException e) {
            log.error("Error occurred", e);
        } finally {
            JmsUtils.closeMessageProducer(producer);
            config.close();
        }
    }
}
