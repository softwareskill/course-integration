package pl.softwareskill.course.integration.messaging.jms;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.support.JmsUtils;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import java.util.UUID;

@Slf4j
public class JmsPublisherTtl {

    public static void main(String[] args) {
        new JmsPublisherTtl().run();
    }

    public void run() {
        JmsConfig config = new JmsConfig();
        MessageProducer producer = null;

        try {
            var clientId = "JMS Publisher " + UUID.randomUUID();
            config.init(clientId);

            Session session = config.getSession();
            log.info("Creating publisher {}", clientId);

            Destination destination = session.createQueue("orders");
            producer = session.createProducer(destination);
            producer.setTimeToLive(5000);

            Message message = session.createTextMessage("message");
            producer.send(message);

            log.info("Message has been sent");
        } catch (JMSException e) {
            log.error("Error occurred", e);
        } finally {
            JmsUtils.closeMessageProducer(producer);
            config.close();
        }
    }
}
