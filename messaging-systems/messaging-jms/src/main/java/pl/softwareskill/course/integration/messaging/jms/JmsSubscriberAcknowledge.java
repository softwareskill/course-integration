package pl.softwareskill.course.integration.messaging.jms;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.support.JmsUtils;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import java.util.UUID;

import static pl.softwareskill.course.integration.messaging.jms.TimeUtils.doInfiniteLoop;

@Slf4j
public class JmsSubscriberAcknowledge {

    public static void main(String[] args) {
        new JmsSubscriberAcknowledge().run();
    }

    public void run() {
        JmsConfig config = new JmsConfig();
        config.setSessionAckMode(Session.CLIENT_ACKNOWLEDGE);
        MessageConsumer consumer = null;

        try {
            var clientId = "JMS Subscriber " + UUID.randomUUID();
            config.init(clientId);
            Session session = config.getSession();

            log.info("Creating subscriber: {}", clientId);

            Destination destination = session.createQueue("orders");
            consumer = session.createConsumer(destination);
            consumer.setMessageListener(new Listener(clientId));

            log.info("Listening...");
            doInfiniteLoop();
        } catch (JMSException e) {
            log.error("Error occurred", e);
        } finally {
            JmsUtils.closeMessageConsumer(consumer);
            config.close();
        }
    }

    @Slf4j
    @RequiredArgsConstructor
    static class Listener implements MessageListener {

        private final String clientId;

        @Override
        public void onMessage(Message message) {
            log.info("Client {} received message: {}", clientId, message);

            try {
                message.acknowledge();
            } catch (JMSException e) {
                log.error("Error while acknowledging message", e);
            }
        }
    }
}
