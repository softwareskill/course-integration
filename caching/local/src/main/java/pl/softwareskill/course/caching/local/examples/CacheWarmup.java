package pl.softwareskill.course.caching.local.examples;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.caching.local.LoggingRemovalListener;
import pl.softwareskill.course.caching.local.data.Data;
import pl.softwareskill.course.caching.local.data.DataSupplier;

import java.time.Duration;
import java.util.Collection;

import static java.util.Arrays.asList;

@Slf4j
@RestController
@Profile("warmup")
public class CacheWarmup {

    private final DataSupplier dataSupplier;
    private final Cache<String, Data> cache;

    public CacheWarmup(DataSupplier dataSupplier) {
        this.dataSupplier = dataSupplier;

        this.cache = Caffeine.newBuilder()
                .refreshAfterWrite(Duration.ofSeconds(30))
                .removalListener(new LoggingRemovalListener<>())
                .build(dataSupplier::get);

        warmup();
    }

    private void warmup() {
        log.info("Warming up cache");

        Collection<String> preloadKeys = asList("key1", "key2", "key3");
        dataSupplier.getMany(preloadKeys)
                .stream()
                .forEach(data -> cache.put(data.getKey(), data));

        log.info("Warming up cache completed");
    }

    @GetMapping("/warmup/{key}")
    public Data run(@PathVariable String key) {
        return cache.get(key, dataSupplier::get);
    }
}
