package pl.softwareskill.course.caching.local.data;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

@Slf4j
@RequiredArgsConstructor
public class CachedDataSupplier {

    public static final String CACHE_NAME = "defaultCacheName";

    private final DataSupplier supplier;

    @Cacheable(CACHE_NAME)
    public Data get(String key) {
        return supplier.get(key);
    }

    @CacheEvict(CACHE_NAME)
    public void onDataUpdated(String key) {
    }
}
