package pl.softwareskill.course.caching.local.data;

import lombok.Value;

import java.time.Instant;

@Value
public class Data {
    String key;
    String value;
    Instant time;
}
