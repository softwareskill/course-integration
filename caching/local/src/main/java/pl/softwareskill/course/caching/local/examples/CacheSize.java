package pl.softwareskill.course.caching.local.examples;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.caching.local.LoggingRemovalListener;
import pl.softwareskill.course.caching.local.data.Data;
import pl.softwareskill.course.caching.local.data.DataSupplier;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CacheSize {

    private final DataSupplier dataSupplier;
    private final Cache<String, Data> cache = Caffeine.newBuilder()
            .maximumSize(2)
            .removalListener(new LoggingRemovalListener<>())
            .build();

    @GetMapping("/size-eviction/{key}")
    public Data run(@PathVariable String key) {
        return cache.get(key, dataSupplier::get);
    }
}
