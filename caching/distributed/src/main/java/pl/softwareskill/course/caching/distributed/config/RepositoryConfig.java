package pl.softwareskill.course.caching.distributed.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.caching.distributed.model.Product;
import pl.softwareskill.course.caching.distributed.model.ProductRepository;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

@Configuration
class RepositoryConfig {

    @Bean
    ProductRepository productRepository(Map<UUID, Product> productsCache) {
        return new CacheProductRepository(productsCache);
    }
}
