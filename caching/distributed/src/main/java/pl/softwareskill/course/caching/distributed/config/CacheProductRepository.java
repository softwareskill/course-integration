package pl.softwareskill.course.caching.distributed.config;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.caching.distributed.model.Product;
import pl.softwareskill.course.caching.distributed.model.ProductRepository;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

@RequiredArgsConstructor
class CacheProductRepository implements ProductRepository {

    private final Map<UUID, Product> productsCache;

    @Override
    public void add(Product product) {
        productsCache.put(product.getId(), product);
    }

    @Override
    public void remove(UUID productId) {
        productsCache.remove(productId);
    }

    @Override
    public Collection<Product> findAll() {
        return productsCache.values();
    }
}
