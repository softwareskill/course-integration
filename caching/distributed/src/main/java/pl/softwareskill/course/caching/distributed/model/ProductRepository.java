package pl.softwareskill.course.caching.distributed.model;

import java.util.Collection;
import java.util.UUID;

public interface ProductRepository {

    void add(Product product);
    void remove(UUID productId);
    Collection<Product> findAll();
}
