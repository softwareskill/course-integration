package pl.softwareskill.course.integration.retry;

import io.github.resilience4j.core.functions.CheckedSupplier;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;

import static io.github.resilience4j.retry.Retry.decorateCheckedSupplier;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@Slf4j
@ExtendWith(MockitoExtension.class)
class RetryTest {

    @Mock
    OrderService orderService;

    @Test
    void recoversFromFailingService() throws Throwable {
        given(orderService.createOrder())
                .willThrow(new RuntimeException("exception 1"))
                .willThrow(new RuntimeException("exception 2"))
                .willReturn("uuid");

        assertThatCode(() -> createOrder().get())
                .doesNotThrowAnyException();

        then(orderService).should(times(3))
                .createOrder();
    }

    private CheckedSupplier<String> createOrder() {
        RetryConfig config = RetryConfig.custom()
                .maxAttempts(5)
                .waitDuration(Duration.ofMillis(200))
                .build();

        Retry retryingOrderService = Retry.of("OrderService", config);

        return decorateCheckedSupplier(retryingOrderService, () -> {
            log.info("creating an order");
            return orderService.createOrder();
        });
//        return decorateCheckedSupplier(retryingOrderService, orderService::createOrder);
    }
}
