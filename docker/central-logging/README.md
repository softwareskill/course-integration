1. Uruchom stos graylog'a `docker-compose up -d `
2. W źródłach (Inputs) dodaj GELF HTTP
3. Wyślij testową wiadomość:

```
curl -X POST -H 'Content-Type: application/json' -d '{ "version": "1.1", "host": "example.org", "short_message": "A short message", "level": 5, "_some_info": "foo" }' 'http://localhost:12201/gelf'
```
